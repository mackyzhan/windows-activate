package activate.util;

import java.io.IOException;

import activate.sys.CmdService;

public class MyUitl {

	public static String getSysEdition() throws IOException {
		String sysEditionReg = CmdService.exec(
				"cmd /c reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\" /v \"EditionID\"");
		String[] sysEditionRegValueTemp = sysEditionReg.split(" ");
		String sysEdition = sysEditionRegValueTemp[sysEditionRegValueTemp.length - 1].replace("\n", "").replace(" ",
				"");
		return sysEdition;
	}

	public static String getSysVersion() throws IOException {
		String sysVersionReg = CmdService.exec(
				"cmd /c reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\" /v \"BuildLab\"");
		String[] sysVersionRegValueTemp = sysVersionReg.split(" ");
		String sysVersion = sysVersionRegValueTemp[sysVersionRegValueTemp.length - 1].replace("\n", "").replace(" ",
				"");
		return sysVersion;
	}

	public static String getActivatieStatus() throws IOException {
		String sysLicense = CmdService.exec("cmd /c cscript /nologo C:\\Windows\\System32\\slmgr.vbs -xpr");
		String sysLicenseValue = sysLicense.substring(sysLicense.indexOf("\n"), sysLicense.length()).replace("\n", "")
				.replace(" ", "").replace("��", "").replace(".", "");
		return sysLicenseValue;
	}

}
