package activate.ui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.swing.border.TitledBorder;

import activate.common.Static;
import activate.exec.win7.Win7OemActivatie;
import activate.util.MyUitl;

import java.awt.BorderLayout;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class Win7OemActivateFrame extends JFrame {

	private static final long serialVersionUID = 9171456491624210724L;
	private static volatile Win7OemActivateFrame instance;

	public JTextArea textArea;
	public JLabel sysLicenseLabel;
	public JButton runActivatieButton;
	public JProgressBar progressBar;

	private JLabel sysEditionLabel;
	private JLabel sysVersionLabel;
	private JComboBox<String> keyComboBox;
	private JLabel activateDiskTitleLabel;
	private JComboBox<String> activateDiskComboBox;

	public Win7OemActivateFrame() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Win7OemActivateFrame.class.getResource("/activate/resource/img/win7-key.png")));

		Font yaheiFont13 = new Font("微软雅黑", Font.PLAIN, 13);
		Font songFont13 = new Font("宋体", Font.PLAIN, 13);

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);

		/* 系统信息 */
		JPanel sysInfoPanel = new JPanel();
		sysInfoPanel.setBorder(
				new TitledBorder(null, "\u7CFB\u7EDF\u4FE1\u606F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		sysInfoPanel.setLayout(null);

		// 系统类型
		sysEditionLabel = new JLabel("系统类型：正在获取...");
		sysEditionLabel.setFont(yaheiFont13);
		sysEditionLabel.setBounds(15, 25, 349, 18);
		sysInfoPanel.add(sysEditionLabel);

		// 系统版本
		sysVersionLabel = new JLabel("系统版本：正在获取...");
		sysVersionLabel.setFont(yaheiFont13);
		sysVersionLabel.setBounds(15, 53, 349, 18);
		sysInfoPanel.add(sysVersionLabel);

		// 激活状态
		sysLicenseLabel = new JLabel("激活状态：正在获取...");
		sysLicenseLabel.setFont(yaheiFont13);
		sysLicenseLabel.setBounds(15, 81, 349, 18);
		sysInfoPanel.add(sysLicenseLabel);
		/* -END- 系统信息 */

		/* 激活选项 */
		JPanel activatieOptionPanel = new JPanel();
		activatieOptionPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"\u6FC0\u6D3B\u9009\u9879\uFF08\u6682\u53EA\u63D0\u4F9B\u8054\u60F3\u8BC1\u4E66\uFF09",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		activatieOptionPanel.setLayout(null);

		JLabel brandTitleLabel = new JLabel("\u54C1\u724C\uFF1A");
		brandTitleLabel.setFont(yaheiFont13);
		brandTitleLabel.setBounds(15, 25, 42, 18);
		activatieOptionPanel.add(brandTitleLabel);

		JComboBox<String> brandComboBox = new JComboBox<String>();
		brandComboBox.setEnabled(false);
		brandComboBox.setFont(songFont13);
		brandComboBox.setFocusable(false);
		brandComboBox.setBounds(67, 22, 118, 24);
		for (String brand : Static.WIN7_OEM_LIC.keySet()) {
			brandComboBox.addItem(brand);
		}
		brandComboBox.setSelectedIndex(0);
		activatieOptionPanel.add(brandComboBox);

		JLabel keyTitleLabel = new JLabel("key\uFF1A");
		keyTitleLabel.setFont(yaheiFont13);
		keyTitleLabel.setBounds(15, 59, 42, 18);
		activatieOptionPanel.add(keyTitleLabel);

		keyComboBox = new JComboBox<String>();
		keyComboBox.setEnabled(false);
		keyComboBox.setFont(songFont13);
		keyComboBox.setFocusable(false);
		keyComboBox.setBounds(67, 56, 118, 24);
		activatieOptionPanel.add(keyComboBox);
		/* -END- 激活选项 */

		/* 引导盘符 */
		JPanel activeDiskPanel = new JPanel();
		activeDiskPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"\u5F15\u5BFC\u76D8\u7B26", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		activeDiskPanel.setLayout(null);

		activateDiskTitleLabel = new JLabel("");
		activateDiskTitleLabel.setFont(yaheiFont13);
		activateDiskTitleLabel.setBounds(15, 25, 139, 18);
		activeDiskPanel.add(activateDiskTitleLabel);

		activateDiskComboBox = new JComboBox<String>();
		activateDiskComboBox.setFont(songFont13);
		activateDiskComboBox.setFocusable(false);
		activateDiskComboBox.setBounds(15, 56, 60, 24);
		activeDiskPanel.add(activateDiskComboBox);
		/* -END- 引导盘符 */

		/* 激活进度 */
		JPanel activatieProcessPanel = new JPanel();
		activatieProcessPanel.setBorder(
				new TitledBorder(null, "\u6FC0\u6D3B\u8FDB\u5EA6", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		activatieProcessPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		activatieProcessPanel.add(scrollPane, BorderLayout.CENTER);

		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		scrollPane.setViewportView(textArea);
		/* --END 激活进度 */

		runActivatieButton = new JButton("\u6FC0\u6D3B");
		runActivatieButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String lic = Static.WIN7_OEM_LIC.get(brandComboBox.getSelectedItem());
				String key = Static.WIN7_OEM_KEY.get(keyComboBox.getSelectedItem());
				if (lic != null && key != null) {
					new Thread(new Win7OemActivatie(lic, key, (String) activateDiskComboBox.getSelectedItem())).start();
				} else {
					JOptionPane.showMessageDialog(Win7OemActivateFrame.getIstance(), "不支持的系统版本");
				}
			}
		});
		runActivatieButton.setFont(yaheiFont13);
		runActivatieButton.setFocusPainted(false);

		progressBar = new JProgressBar();

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addGap(10)
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel.createSequentialGroup()
						.addComponent(sysInfoPanel, GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE).addContainerGap())
						.addGroup(gl_panel.createSequentialGroup()
								.addComponent(activatieProcessPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addContainerGap())
						.addGroup(gl_panel.createSequentialGroup().addGroup(gl_panel
								.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel.createSequentialGroup()
										.addComponent(activatieOptionPanel, GroupLayout.DEFAULT_SIZE, 205,
												Short.MAX_VALUE)
										.addGap(10).addComponent(activeDiskPanel, GroupLayout.PREFERRED_SIZE, 164,
												GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel.createSequentialGroup().addGap(5)
										.addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 289, Short.MAX_VALUE)
										.addGap(10).addComponent(runActivatieButton, GroupLayout.PREFERRED_SIZE, 75,
												GroupLayout.PREFERRED_SIZE)))
								.addGap(12)))));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addContainerGap()
				.addComponent(sysInfoPanel, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE).addGap(10)
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(activatieOptionPanel, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
						.addComponent(activeDiskPanel, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
				.addGap(10).addComponent(activatieProcessPanel, GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)
				.addGap(10)
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup().addGap(1).addComponent(progressBar,
								GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
						.addComponent(runActivatieButton, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
				.addGap(11)));
		panel.setLayout(gl_panel);

		/* 获取系统信息 */
		this.getSystemInfo();
		/* -END- 获取系统信息 */

		this.setTitle("Windows 7 OEM\u6FC0\u6D3B");
		this.setSize(new Dimension(415, 500));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - this.getWidth()) / 2,
				(Toolkit.getDefaultToolkit().getScreenSize().height - this.getHeight()) / 2);
		this.setVisible(true);
		this.setResizable(true);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	private void getSystemInfo() {
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				// 系统版本
				String sysEdition = null;
				String sysVersion = null;
				try {
					sysEdition = MyUitl.getSysEdition();
					sysVersion = MyUitl.getSysVersion();
					sysEditionLabel.setText("系统类型：" + sysEdition);
					sysVersionLabel.setText("系统版本：" + sysVersion);
				} catch (IOException e) {
					sysEditionLabel.setText("系统类型获取失败");
					sysVersionLabel.setText("系统版本获取失败");
					e.printStackTrace();
				}
				if (sysEdition != null) {
					String verName = Static.WIN7_SYS_VERSION.get(sysEdition);
					for (String key : Static.WIN7_OEM_KEY.keySet()) {
						if (key.contains(verName)) {
							keyComboBox.addItem(key);
						}
					}
					keyComboBox.setSelectedIndex(0);
				}
				// 引导盘符
				String[] diskList = new String[] { "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
						"Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
				ArrayList<String> bootPaths = new ArrayList<String>();
				for (int i = 0; i < diskList.length; i++) {
					File bootmgrFile = new File(diskList[i] + ":\\bootmgr");
					if (bootmgrFile.exists() && bootmgrFile.isFile()) {
						bootPaths.add(diskList[i]);
					}
				}
				switch (bootPaths.size()) {
				case 0:
					activateDiskTitleLabel.setText("无活动分区");
					activateDiskComboBox.setEnabled(false);
					break;
				case 1:
					activateDiskTitleLabel.setText("只有一个，已自动选择");
					activateDiskComboBox.addItem(bootPaths.get(0));
					activateDiskComboBox.setSelectedItem(0);
					activateDiskComboBox.setEnabled(false);
					break;
				default:
					activateDiskTitleLabel.setText("有多个，需手动选择");
					for (int i = 0; i < bootPaths.size(); i++) {
						activateDiskComboBox.addItem(bootPaths.get(i));
					}
					activateDiskComboBox.setSelectedItem(0);
					break;
				}
				// 激活状态
				try {
					String sysLicenseValue = MyUitl.getActivatieStatus();
					if (!"".equals(sysLicenseValue)) {
						sysLicenseLabel.setText("激活状态：" + sysLicenseValue);
					} else {
						sysLicenseLabel.setText("激活状态：未找到密钥");
					}
				} catch (Exception e) {
					sysLicenseLabel.setText("激活状态：获取失败");
					e.printStackTrace();
				}
				return null;
			}

		};
		worker.execute();

	}

	public void consoleLog(String s) {
		textArea.append(s + "\r\n");
		textArea.setCaretPosition(textArea.getText().length());
	}

	public static Win7OemActivateFrame getIstance() {
		if (instance == null) {
			synchronized (Win7OemActivateFrame.class) {
				if (instance == null) {
					instance = new Win7OemActivateFrame();
				}
			}
		}
		return instance;
	}
}
